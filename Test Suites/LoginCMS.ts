<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>LoginCMS</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1c6f1f87-fad3-4252-b7e6-17cd175cbc3a</testSuiteGuid>
   <testCaseLink>
      <guid>63d5372e-ae9c-4b7b-9c7e-12f365c2f621</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login-CMS-UI/forgotpass-test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>416f037e-ea20-4561-a67e-90734e6f4f32</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login-CMS-UI/logout-test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96556af3-d176-4090-b1f8-83d4878cfce4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login-CMS-UI/negatif-test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c4fec88-c2a3-4d69-be50-24167f53af70</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login-CMS-UI/positif-test</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
